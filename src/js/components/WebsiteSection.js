class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    const template = document.getElementById('section-template');

    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(template.content.cloneNode(true));

    const heading = this.shadowRoot.querySelector('.app-title');
    heading.innerHTML = this.getAttribute('title');
    const description = this.shadowRoot.querySelector('.app-subtitle');
    description.innerHTML = this.getAttribute('description');
  }
}

customElements.define('website-section', WebsiteSection);
