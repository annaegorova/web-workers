import { SectionCreator } from './components/SectionCreator.js';
import '../styles/style.css';
import './components/WebsiteSection';

document.addEventListener('DOMContentLoaded', () => {
  SectionCreator.create('community').render();
  SectionCreator.create('standard').render();
  const worker = new Worker(new URL('./worker.js', import.meta.url));
  const clickableElements = document.querySelectorAll('button, input');

  clickableElements.forEach(element => {
    element.addEventListener('click', event => {
      worker.postMessage(event.target.nodeName);
    });
  });
});

